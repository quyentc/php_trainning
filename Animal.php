<?php
require_once ('Fly.php');
// Abstract class
abstract class Animal
{
    protected $Name;
    protected $Type;
    public $eat;

    public function __construct($Name, $Type)
    {
        $this->Name = $Name;
        $this->Type = $Type;

    }
    public function __toString()
    {
        return printf("Name: %s \nType: %s", $this->Name,$this->Type);
    }

    //Abstract function
    abstract protected function Run();
    abstract protected function getLike();
    abstract protected function setLike($like);

}
//ke thua
class Dog extends Animal
{
    private $Like;

    protected function Run()
    {
        return "Chu cho chay rat gioi";
    }



    public function __toString()
    {
        $this->eat="dang an";
        return parent::__toString().printf("\n%s \n%s \n%s",$this->eat, $this->Run(), $this->getLike());
    }

    protected function getLike()
    {
        return $this->Name." rat thich ".$this->Like;
    }

    public function setLike($like)
    {
        $this->Like = $like;
    }
}

class Bird extends Animal implements Fly
{

    protected function Run()
    {
        return ;
    }

    protected function getLike()
    {
        return $this->Name." rat thich ".$this->Like;
    }

    public function setLike($like)
    {
        $this->Like = $like;
    }
    public function Bay()
    {
        return $this->Name." bay luon rat gioi";
    }
    public function __toString()
    {
        return parent::__toString().printf("\n%s \n%s", $this->Bay(), $this->getLike());
    }



}

$dog = new Dog('Ari','chihuahua');
$dog->setLike("thit");
$dog->__toString();

//$bird= new Bird("Aray","dai bang");
//$bird->setLike("san moi");
//$bird->__toString();
