<?php


abstract class ChiTietMay
{
    protected $MaSo;
    protected function Nhap()
    {
        print ("\nNhap ma so may:");
        fscanf(STDIN, "%s", $this->MaSo);

    }
    protected function Xuat()
    {
        printf("Ma so %s ", $this->MaSo);
    }

    /**
     * @return mixed
     */
    public function getMaSo()
    {
        return $this->MaSo;
    }

    abstract protected function TinhTien();
    abstract protected function TinhKhoiLuong();
}