<?php



class ChiTietPhuc extends ChiTietMay {
    public $DScon=array();
    public $totalPrice=array();
    public $totalWeight=array();
    public $SLcon=0;
    public $loai;
    public $item;


    public function Nhap()
    {
        parent::Nhap();
        print ("\nNhap so luong chi tiet con: ");
        fscanf(STDIN, "%i", $this->SLcon);
        if (!$this->ValidNumber($this->SLcon))
        {
            print ("\nBan hay nhap 1 con so chinh xac\n ");
            print ("\nNhap so luong chi tiet con: ");
            fscanf(STDIN, "%i", $this->SLcon);
            $this->OptionCT();
        }else {
                $this->OptionCT();
        }

    }
    //chon loai chi tiet, neu chon khong dung thi chay lai
    public function OptionCT()
    {
        for ($i = 0; $i < $this->SLcon; $i++) {
            print ("\nChon chi tiet (1: don, 2: phuc ): ");
            fscanf(STDIN, "%i", $this->loai);

            if ($this->loai != 1 && $this->loai != 2) {
                print ("Vui long lua chon dung nhu da neu phia tren");
                $this->OptionCT();
            } else {
                if ($this->loai == 1) {
                    $this->item = new ChiTietDon();
                    $this->PushArr($this->item);
                } else if ($this->loai == 2) {
                    $this->item = new ChiTietPhuc();
                    $this->PushArr($this->item);
                }
            }
        }

    }

    public function PushArr($item)
    {
        $item->Nhap();
        array_push($this->DScon, $item);
        array_push($this->totalPrice, $item->TinhTien());
        array_push($this->totalWeight, $item->TinhKhoiLuong());
    }

    public function Xuat()
    {
        parent::Xuat(). printf(" co %i chi tiet con \n",$this->SLcon);

        printf("\nTong tien: %g", $this->TinhTien());
        printf("\nTong khoi luong: %g", $this->TinhKhoiLuong());


    }

    /**
     * @return array
     */
    public function getDScon(): array
    {
        return $this->DScon;
    }

    public function TinhTien()
    {
        return array_sum($this->totalPrice);
    }

    public function TinhKhoiLuong()
    {
        return array_sum($this->totalWeight);
    }

    public function ValidNumber($value)
    {
        if ($value>0 && $value <=2)
            return true;
        return false;

    }
}
//
//$o = new ChiTietPhuc();
//$o->Nhap();
//$o->Xuat();