<?php


class ChiTietDon extends ChiTietMay
{

    public $GiaTien;
    public $KhoiLuong;

    public function Nhap()
    {
        parent::Nhap();
        print ("Nhap gia tien:");
        fscanf(STDIN, "%g", $this->GiaTien);
        $this->ValidNumber($this->GiaTien);
        print ("Nhap khoi luong:");
        fscanf(STDIN, "%g", $this->KhoiLuong);
        $this->ValidNumber($this->KhoiLuong);

    }
    public function ValidNumber($value)
    {
        if (is_numeric($value)) { //return false if string and true is number
            return true;
        }
        return false;
    }
    public function Xuat()
    {
        parent::Xuat();
        printf("\nGia tien %g \n", $this->GiaTien);
        printf("Khoi luong %g \n", $this->KhoiLuong);
    }


    protected function TinhTien()
    {
        return $this->GiaTien;
    }

    protected function TinhKhoiLuong()
    {
        return $this->KhoiLuong;
    }
}

