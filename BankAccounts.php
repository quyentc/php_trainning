<?php


class BankAccounts
{
    public $email;
    public $name;
    public $pass;
    public $amount;
    private $balance;

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }


    private $listAccounts = [
        [
            'email' => 'ari@example.net',
            'pass' => 'ari123',
        ],
        [
            'email' => 'mary@example.net',
            'pass' => 'mary123',
        ]
    ];

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    public function authenticate()
    {
        foreach ($this->getListAcc() as $acc)
        {
            if (($acc['email'] == $this->email) && $acc['pass'] == $this->pass)
            {
                return true;
            }
            else {
                return false;
            }
        }
    }
    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        if (!$this->authenticate())
        {
            echo "Error!";
            die();
        }
        else {
            $this->balance = $balance;
        }
    }

    /**
     * @return array
     */
    public function getListAcc(): array
    {
        return $this->listAccounts;
    }

    //tinh truu tuong
    public function naptien()
    {
        if (!$this->authenticate())
        {
            echo "Error!";
            die();
        }
        if ($this->amount<50000)
        {
            echo 'nap tien gia tri lon hon 50000';
        }
        else{
            $this->balance+=$this->amount;
        }
    }




}

$a= new BankAccounts();
$a->name = "Ari";
$a->email = "ari@example.net";
$a->pass = "ari123";
//tinh dong goi
$a->setBalance(1000);
$a->setAmount(600000);
$a->naptien();
echo $a->getBalance();